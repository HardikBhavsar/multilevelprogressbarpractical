import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import MultiLevelProgressBar from './src/Components/MultiLevelProgressBar';

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>          
          <View style={styles.sectionContainer}>
            <Text style={styles.sectionTitle}>
              Hardik's Practical
            </Text>
            <Text
            style={styles.title}>
              Multi-Level Progress Bar
            </Text>
          </View>
          <View style={styles.body}>
            <MultiLevelProgressBar />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "white",
  },
  sectionContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: "black",
  },
  body: {
    backgroundColor: "white",    
  },
  title: {
    fontSize: 20,
    alignSelf: 'center',
    paddingBottom: 20,
  },    
});

export default App;
