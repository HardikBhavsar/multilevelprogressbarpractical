import React, { useState } from 'react';
import {     
    View,
    StyleSheet,
    Image,    
    Animated,
    Easing,
    TouchableOpacity,
    Text,
} from 'react-native';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

export default class MultiLevelProgressBar extends React.Component {

    state: any;        
    
    constructor(props) {
      super(props);      
      this.state = {        
        progressAnim: new Animated.Value(10),
        fadeAnim: new Animated.Value(0),                        
        fillAnimation: new Animated.Value(0),
      };            
      this.animate();      
    }

    _onPress = () => {
        Animated.sequence([

            Animated.timing(this.state.fillAnimation, {
                toValue: 0,
                duration: 100, 
                useNativeDriver: false,           
            }),   

            Animated.timing(this.state.fadeAnim, {
                toValue: 1,
                duration: 300, 
                useNativeDriver: false,           
            }),                   

            Animated.timing(this.state.fillAnimation, {
                toValue: 100,
                duration: 1500, 
                useNativeDriver: false,           
            }),   
            
            Animated.timing(this.state.fadeAnim, {
                toValue: 0,
                duration: 700, 
                useNativeDriver: false,           
            }),

            Animated.timing(this.state.progressAnim, {
                toValue: 290,
                duration: 900, 
                useNativeDriver: false,           
            }),
    
        ]).start();
    }   
    
    animate(){    
        
        Animated.sequence([

            Animated.timing(this.state.progressAnim, {
                toValue: 145,
                duration: 2000, 
                useNativeDriver: false,           
            }),

            Animated.timing(this.state.fadeAnim, {
                toValue: 1,
                duration: 300, 
                useNativeDriver: false,           
            }),                   

            Animated.timing(this.state.fillAnimation, {
                toValue: 100,
                duration: 1500, 
                useNativeDriver: false,           
            }),   
            
            Animated.timing(this.state.fadeAnim, {
                toValue: 0,
                duration: 700, 
                useNativeDriver: false,           
            }),

            Animated.timing(this.state.progressAnim, {
                toValue: 210, //290
                duration: 900, 
                useNativeDriver: false,           
            }),
    
        ]).start();

    } 

    render() {
            return (
                <View style={styles.container}>      
                <Image 
                    style={styles.bottomLayer}
                    source={require('../../img/basic-frame.png')} 
                />
                <View 
                    style={styles.progressContainer1}>
                    <Animated.Image 
                        style={styles.progressBar, {width: this.state.progressAnim}}
                        source={require('../../img/content.png')}
                    />
                    <Image 
                        style={styles.closedCircle}
                        source={require('../../img/closed-circle-progress-bar.png')} 
                    />                
                </View>
                <View 
                    style={styles.progressContainer2}>
                    <Animated.Image 
                        style={styles.openCircle, {opacity: this.state.fadeAnim}}
                        source={require('../../img/circle-progress-bar.png')}
                    />                                
                    <AnimatedCircularProgress
                        size={50}
                        width={25}
                        rotation={0}                                                
                        fill={this.state.fillAnimation}                    
                        tintColor="#3377ff"                                        
                        style={{position: 'absolute', opacity: this.state.fadeAnim}}
                        easing={Easing.in(Easing.bounce)}
                        duration={5}
                    />
                </View>                

                <TouchableOpacity onPress={this._onPress}>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>Complete me!</Text>
                    </View>
                </TouchableOpacity>

                </View>
            )        
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 50,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",        
        height: 200,
    },
    title: {
        fontSize: 30
    },
    bottomLayer: {        
        position: 'absolute',
        margin: 50,
        justifyContent: "center",
        width: 300,
        height: 200,
        resizeMode: 'contain',            
    },
    progressContainer1: {
        position: 'absolute',
        justifyContent: "flex-start",
        alignItems: 'center',
        width: 290,        
        height: 100,
        flexDirection: 'row',
    },    
    progressBar: {        
        width: 290,        
        height: 20,        
        resizeMode: 'stretch',        
    },
    closedCircle: {                
        width: 40,
        height: 100,
        resizeMode: 'contain',    
        marginLeft: -15,
    },
    progressContainer2: {
        position: 'absolute',
        justifyContent: "center",
        alignItems: 'center',
        width: 80,        
        height: 80,        
    }, 
    openCircle: {   
        position: 'absolute',     
        width: 50,        
        height: 50,        
        resizeMode: 'contain',    
    },
    button: {
        backgroundColor: '#3377ff',
        paddingHorizontal: 20,
        paddingVertical: 15,
        marginTop: 90,
    },
      buttonText: {
        color: 'white',
        fontWeight: 'bold',
    },    
});

export default MultiLevelProgressBar;
